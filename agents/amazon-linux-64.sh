#!/bin/bash

yum install -y flex bison boost-devel doxygen s3cmd expect gdb elfutils

# valgrind38
# The default version (3.6) does not work.
yum install -y ftp://rpmfind.net/linux/centos/6.5/os/x86_64/Packages/valgrind-3.8.1-3.2.el6.x86_64.rpm

# rpmbuild48
# We cannot install the rpmbuild that ships because it has an dependency conflict.
rpm --nodeps -i ftp://ftp.sunet.se/pub/Linux/distributions/scientific/6rolling/x86_64/os/Packages/rpm-build-4.8.0-32.el6.x86_64.rpm

# argparse
easy_install argparse

# fix up git
git config --global user.name "Bamboo"
git config --global user.email bamboo@elliot.atlassian.net
